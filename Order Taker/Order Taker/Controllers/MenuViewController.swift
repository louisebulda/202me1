//
//  MenuViewController.swift
//  Order Taker
//
//  Created by Louise Bulda on 10/28/21.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, MenuDelegate {
    lazy var selectedCells = [IndexPath]() // list to get number of items selected
    lazy var selectedItems = [MenuDetails]()
    
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var cartNumberItems: UILabel?
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var checkoutButton: UIButton!
    
    let menuPresenter = MenuPresenter()
    let menuDatabase = MenuDatabase().addMenu()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        
        menuTable.delegate = self
        menuTable.dataSource = self
        
        menuPresenter.setMenuDelegate(menuDelegate: self)
    }
    
    @IBAction func clearButtonClicked(_ sender: Any) {
        selectedCells = menuPresenter.clearCellItems(selectedCells: selectedCells)
        selectedItems = menuPresenter.clearItems(selectedItems: selectedItems)
    }
    
    @IBAction func checkoutButtonClicked(_ sender: Any) {
        menuPresenter.checkoutItems(checkoutButton: checkoutButton, selectedCells: selectedCells)
    }

    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        selectedCells = menuPresenter.clearCellItems(selectedCells: selectedCells)
        selectedItems = menuPresenter.clearItems(selectedItems: selectedItems)
    }
    
    func displayNumberOfItems() {
        cartNumberItems?.text = "(N) item(s) in cart"
    }
    
    func didCheckoutSegue(){
        performSegue(withIdentifier: "checkoutSegue", sender: self)
    }
}

extension MenuViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuDatabase.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuDatabase[section].menuList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuItem = menuDatabase[indexPath.section].menuList[indexPath.row]
        
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuTableViewCell
        
        menuCell.nameLabel?.text = "\(menuItem.name)"
        menuCell.sizeLabel?.text = "(\(menuItem.size))"
        menuCell.priceLabel?.text = "P\(menuItem.price)"
        
        return menuCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return menuDatabase[section].sectionName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuDatabase[indexPath.section].menuList[indexPath.row]
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedCells.append(indexPath)
        
        selectedItems.append(menuItem)
        
        if selectedCells.contains(indexPath) {
            cartNumberItems?.text = "(\(selectedCells.count)) item(s) in cart"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checkoutSegue" {
            if let destination = segue.destination as? CheckoutTableViewController {
                destination.checkoutSelected = selectedItems
            }
        }
    }
}

