//
//  CheckoutTableViewController.swift
//  Order Taker
//
//  Created by Louise Bulda on 11/2/21.
//

import UIKit

class CheckoutTableViewController: UITableViewController, CheckoutDelegate {
    var navigationTitle  = "Checkout"
    var checkoutSelected = [MenuDetails]()
    // var selectedPrice = [Double]()
    var accessSum: Double = 0.0
    var accessPayment: Double = 0.0
    
    let checkoutPresenter = CheckoutPresenter()
    
    @IBOutlet weak var checkoutTable: UITableView!
    @IBOutlet weak var totalPriceLabel: UILabel?
    @IBOutlet weak var paymentInput: UITextField!
    @IBOutlet weak var payButton: UIButton!
    
    
    @IBAction func payButtonClicked(_ sender: Any) {
        accessPayment = checkoutPresenter.convertPayment(payment: paymentInput.text ?? "")
        checkoutPresenter.payItems(paymentDouble: accessPayment, accessSum: accessSum, payButton: payButton)
    }
    
    func didReceiptSegue() {
        performSegue(withIdentifier: "receiptSegue", sender: self)
    }
    
    func displayTotalPrice(sum: Double) {
        totalPriceLabel?.text = "P \(sum)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title =  navigationTitle
        
        checkoutTable.delegate = self
        checkoutTable.dataSource = self
        
        checkoutPresenter.setCheckoutDelegate(checkoutDelegate: self)
        
        accessSum = checkoutPresenter.sumItems(checkoutSum: checkoutSelected)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkoutSelected.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let checkoutItem = checkoutSelected[indexPath.row]
        
        let checkoutCell = tableView.dequeueReusableCell(withIdentifier: "checkoutCell", for: indexPath) as! MenuTableViewCell
        
        checkoutCell.nameLabel?.text = "\(checkoutItem.name)"
        checkoutCell.sizeLabel?.text = "(\(checkoutItem.size))"
        checkoutCell.priceLabel?.text = "P\(checkoutItem.price)"
        
        return checkoutCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "receiptSegue" {
            if let destination = segue.destination as? ReceiptTableViewController {
                destination.receiptSelected = checkoutSelected
                destination.receiptSum = accessSum
                destination.receiptPayment = accessPayment
            }
        }
    }
}


