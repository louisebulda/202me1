//
//  ReceiptTableViewController.swift
//  Order Taker
//
//  Created by Louise Bulda on 11/5/21.
//

import UIKit

class ReceiptTableViewController: UITableViewController {
    var navigationTitle  = "Receipt"
    var receiptSelected = [MenuDetails]()
    var receiptSum: Double = 0.0
    var receiptPayment: Double = 0.0
    
    @IBOutlet weak var totalPriceReceiptLabel: UILabel?
    @IBOutlet weak var paymentReceivedLabel: UILabel?
    @IBOutlet weak var changeLabel: UILabel?
    
    
    @IBOutlet weak var restaurantImage: UIImageView!
    
    
    @IBAction func doneClicked(_ sender: Any) {
        performSegue(withIdentifier: "unwindToMenu", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title =  navigationTitle
        
        let changeGiven: Double = receiptPayment - receiptSum
        
        totalPriceReceiptLabel?.text = "P\(receiptSum)"
        paymentReceivedLabel?.text = "P\(receiptPayment)"
        changeLabel?.text = "P\(changeGiven)"
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receiptSelected.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let receiptItem = receiptSelected[indexPath.row]
        
        let receiptCell = tableView.dequeueReusableCell(withIdentifier: "receiptCell", for: indexPath) as! MenuTableViewCell
        
        receiptCell.nameLabel?.text = "\(receiptItem.name)"
        receiptCell.sizeLabel?.text = "(\(receiptItem.size))"
        receiptCell.priceLabel?.text = "P\(receiptItem.price)"
        
        return receiptCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
