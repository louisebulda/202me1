//
//  Menu.swift
//  Order Taker
//
//  Created by Louise Bulda on 10/28/21.
//

import Foundation
import UIKit

// Class used to define the details of the menu item 
class MenuDetails {
    let name: String
    let size: String
    let price: Double
    
    
    init(_ name: String, _ size: String, _ price: Double){
        self.name = name
        self.size = size
        self.price = price
    }
}
