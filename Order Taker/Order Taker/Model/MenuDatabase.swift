//
//  MenuDatabase.swift
//  Order Taker
//
//  Created by Louise Bulda on 10/29/21.
//

import Foundation

protocol MenuDatabaseProtocol: class {
    func addMenu() -> [MenuData]
}

// Class to separate the menu items (food or beverage)
class MenuData {
    
    let sectionName: String
    let menuList: [MenuDetails]
    
    init(sectionName: String, menuList: [MenuDetails]){
        self.sectionName = sectionName
        self.menuList = menuList
    }
}

class MenuDatabase: MenuDatabaseProtocol {
    var menu = [MenuData]() // menu list to append menu items according to section
    
    func addMenu() -> [MenuData] {
        menu.append(MenuData.init(sectionName: "Food", menuList: [
            MenuDetails("Caesar Salad", "Solo", 225.0),
            MenuDetails("Traditional New England Seafood Chowder", "Solo", 300.0),
            MenuDetails("French Onion Soup", "Solo", 199.0),
            MenuDetails("Chicken Pot Pie", "Meal", 350.0),
            MenuDetails("Bacon Cheeseburger", "Meal", 270.0),
            MenuDetails("Buffalo Wings", "Solo", 290.0),
            MenuDetails("Grilled Ham and Cheese Sandwich", "Meal", 240.0),
            MenuDetails("Shrimp Aglio e Olio", "Meal", 499.0),
            MenuDetails("Angus Ribeye Steak", "Meal", 1200.0)
        ]))
        
        menu.append(MenuData.init(sectionName: "Beverage", menuList: [
            MenuDetails("Mango Shake", "L", 165.0),
            MenuDetails("Iced Tea", "M", 90.0),
            MenuDetails("Caramel Frappucino", "M", 195.0),
            MenuDetails("Honey Latte", "S", 225.0),
            MenuDetails("Oolong Milk Tea", "M", 130.0),
            MenuDetails("Blue Lemonade", "L", 100.0),
            MenuDetails("Coca Cola", "S", 90.0)
        ]))
        
        return menu
    }
    
}
