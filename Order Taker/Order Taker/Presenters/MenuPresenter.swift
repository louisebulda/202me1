//
//  MenuPresenter.swift
//  Order Taker
//
//  Created by Louise Bulda on 11/26/21.
//

import Foundation
import AMPopTip

protocol MenuDelegate: class {
    func displayNumberOfItems()
    func didCheckoutSegue()
}

class MenuPresenter {
    weak var menuDelegate: MenuDelegate?
    
    func setMenuDelegate (menuDelegate: MenuDelegate?) {
        self.menuDelegate = menuDelegate
    }
    
    func clearCellItems(selectedCells: [IndexPath]) -> [IndexPath] {
        var selectedCells = selectedCells
        selectedCells.removeAll()
        
        return selectedCells
    }
    
    func clearItems(selectedItems: [MenuDetails]) -> [MenuDetails] {
        var selectedItems = selectedItems
        selectedItems.removeAll()
        menuDelegate?.displayNumberOfItems()
        
        return selectedItems
    }
    
    func checkoutItems(checkoutButton: UIButton?, selectedCells: [IndexPath]) {
        if selectedCells.count != 0 {
            menuDelegate?.didCheckoutSegue()
        } else {
            let popTip = PopTip()
            let custom = CGRect(x: 0, y: -10, width: 10, height: 10)
            popTip.show(text: "Checkout empty.", direction: .up, maxWidth: 200, in: checkoutButton!, from: custom, duration: 1)
        }
    }
}
