//
//  CheckoutPresenter.swift
//  Order Taker
//
//  Created by Louise Buldaon 11/26/21.
//

import Foundation
import AMPopTip

protocol CheckoutDelegate: class {
    func displayTotalPrice(sum: Double)
    func didReceiptSegue()
}

class CheckoutPresenter {
    weak var checkoutDelegate: CheckoutDelegate?
    
    func setCheckoutDelegate (checkoutDelegate: CheckoutDelegate?) {
        self.checkoutDelegate = checkoutDelegate
    }
    
    func convertPayment(payment: String) -> Double {
        let paymentDouble = Double(payment) ?? 0
        return paymentDouble
    }
    
    func payItems(paymentDouble: Double, accessSum: Double, payButton: UIButton) {
        if paymentDouble >= accessSum {
            checkoutDelegate?.didReceiptSegue()
            print("Paid \(paymentDouble)")
        } else {
            let popTip = PopTip()
            let custom = CGRect(x: 0, y: -10, width: 10, height: 10)
            popTip.show(text: "Insufficient payment.", direction: .up, maxWidth: 200, in: payButton, from: custom, duration: 1)
        }
    }
    
    func sumItems(checkoutSum: [MenuDetails]) -> Double {
        var selectedPrice = [Double]()

        for i in checkoutSum {
            selectedPrice.append(i.price)
        }
        
        let sum = selectedPrice.reduce(0, +)
        checkoutDelegate?.displayTotalPrice(sum: sum)
        
        return sum
    }
}
