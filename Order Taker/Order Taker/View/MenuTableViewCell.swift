//
//  MenuTableViewCell.swift
//  Order Taker
//
//  Created by Louise Bulda on 10/29/21.
//

import Foundation
import UIKit

class MenuTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var sizeLabel: UILabel?
    @IBOutlet var priceLabel: UILabel?
}
