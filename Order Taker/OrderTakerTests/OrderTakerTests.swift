//
//  OrderTakerTests.swift
//  OrderTakerTests
//
//  Created by Louise on 11/26/21.
//

import XCTest
@testable import Order_Taker

class OrderTakerTests: XCTestCase {
    var sut: MenuViewController!
    var newSut: CheckoutTableViewController!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = MenuViewController()
        newSut = CheckoutTableViewController()
    }

    override func tearDownWithError() throws {
        sut = nil
        newSut = nil
        try super.tearDownWithError()
    }
    
    func testCartForIndexClearedWhenClearButtonPressed(){
        let indexPath = IndexPath(row: 0, section: 0)
        let indexPath1 = IndexPath(row: 1, section: 0)
        let indexPath2 = IndexPath(row: 2, section: 1)
    
        sut.selectedCells = [indexPath, indexPath1, indexPath2]
    
        sut.clearButtonClicked((Any).self)

        XCTAssert(sut.selectedCells == [], "Not cleared.")
    }
     
    func testReturnedSum(){
        var checkoutSum: [MenuDetails] = []
        let item1 = MenuDetails("Bacon Cheeseburger", "Meal", 270.0)
        let item2 =  MenuDetails("Caramel Frappucino", "M", 195.0)
        
        checkoutSum.append(item1)
        checkoutSum.append(item2)
        
        let accessSum = newSut.checkoutPresenter.sumItems(checkoutSum: checkoutSum)
        
        XCTAssert(accessSum != 0.0, "No sum returned.")
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
